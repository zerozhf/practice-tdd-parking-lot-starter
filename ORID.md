# Daily Report (2023/07/14)

## O

### Code review

In today's code review session, I found a bad habit of writing code. In previous projects, although I would encapsulate some data and behavior to a certain extent, when dealing with some business logic, in order to keep the code concise Sex, some data will be leaked, which is a particularly bad habit.

### TDD 

Yesterday I learned the concept of TDD and some basic implementation methods. Today, I did a lot of exercises on TDD combined with Java OO. In these exercises, I really found that using TDD can make me aware of many requirements that I would not have noticed before.

## R

meaningful

## I

Gradually became familiar with the process of TDD and realized the advantages of TDD

## D

In the future development practice, if time is more abundant, I will use TDD to develop