package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParkingLotTest {

    @Test
    public void should_return_a_parking_ticket_when_park_given_a_car() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();

        //when
        ParkingTicket parkingTicket = parkingLot.park(car);

        //then
        Assertions.assertNotNull(parkingTicket);

    }

    @Test
    public void should_return_no_available_position_when_park_given_a_car_but_parking_lot_is_full() {
        //given
        ParkingLot parkingLot = new ParkingLot(0);
        //when
        //then
        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> parkingLot.park(new Car()));
        assertEquals("No available position", exception.getMessage());

    }


    @Test
    public void should_return_a_car_when_fetch_given_a_parking_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingTicket parkingTicket = parkingLot.park(new Car());

        //when
        Car car = parkingLot.fetch(parkingTicket);

        //then
        Assertions.assertNotNull(car);

    }

    @Test
    public void should_return_right_car_when_fetch_given_two_parking_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket parkingTicket1 = parkingLot.park(car1);
        ParkingTicket parkingTicket2 = parkingLot.park(car2);

        //when
        Car returnCar1 = parkingLot.fetch(parkingTicket1);
        Car returnCar2 = parkingLot.fetch(parkingTicket2);

        //then
        Assertions.assertEquals(car1, returnCar1);
        Assertions.assertEquals(car2, returnCar2);

    }

    @Test
    public void should_return_a_exception_when_fetch_given_a_unrecongnized_parking_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        parkingLot.park(new Car());

        //when

        //then

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> parkingLot.fetch(null));
        assertEquals("Unrecognized parking ticket", exception.getMessage());

    }

    @Test
    public void should_return_Unrecognized_parking_ticket_when_fetch_given_a_used_parking_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingTicket parkingTicket1 = parkingLot.park(new Car());
        parkingLot.fetch(parkingTicket1);
        //when
        //then
        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> parkingLot.fetch(parkingTicket1));
        assertEquals("Unrecognized parking ticket", exception.getMessage());

    }

    @Test
    public void should_return_a_parking_ticket_of_first_parking_lot_when_park_given_a_car_and_first_parking_lot_has_available_position() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();

        //when
        ParkingTicket parkingTicket = parkingLot.park(car);

        //then
        Assertions.assertNotNull(parkingTicket);

    }



}
