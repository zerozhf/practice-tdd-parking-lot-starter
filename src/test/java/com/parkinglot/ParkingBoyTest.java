package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ParkingBoyTest {

    @Test
    public void should_return_a_parking_ticket_when_park_given_a_parking_lot_and_a_car() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car = new Car();

        //when
        ParkingTicket parkingTicket = parkingBoy.park(car);
        //then
        Assertions.assertNotNull(parkingTicket);


    }

    @Test
    public void should_return_no_available_position_when_park_given_a_parking_lot_and_a_car_but_parking_lot_is_full() {
        //given
        ParkingLot parkingLot = new ParkingLot(0);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car = new Car();
        //when
        //then
        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> parkingBoy.park(car));
        assertEquals("No available position", exception.getMessage());

    }


    @Test
    public void should_return_a_car_when_fetch_given_a_parking_lot_and_a_parking_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car = new Car();
        ParkingTicket parkingTicket = parkingBoy.park(car);

        //when
        Car returnCar = parkingBoy.fetch(parkingTicket);

        //then
        Assertions.assertEquals(car,returnCar);

    }

    @Test
    public void should_return_right_car_when_fetch_given_a_parking_lot_and_two_parking_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        Car car1 = new Car();
        Car car2 = new Car();
        ParkingTicket parkingTicket1 = parkingBoy.park(car1);
        ParkingTicket parkingTicket2 = parkingBoy.park(car2);

        //when
        Car returnCar1 = parkingBoy.fetch(parkingTicket1);
        Car returnCar2 = parkingBoy.fetch(parkingTicket2);

        //then
        Assertions.assertEquals(car1, returnCar1);
        Assertions.assertEquals(car2, returnCar2);

    }

    @Test
    public void should_return_a_exception_when_fetch_given_a_parking_lot_and_a_unrecongnized_parking_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        parkingBoy.park(new Car());

        //when

        //then

        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> parkingBoy.fetch(null));
        assertEquals("Unrecognized parking ticket", exception.getMessage());

    }

    @Test
    public void should_return_Unrecognized_parking_ticket_when_fetch_given_a_parking_lot_and_a_used_parking_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot(10);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLot);
        ParkingTicket parkingTicket1 = parkingBoy.park(new Car());
        parkingBoy.fetch(parkingTicket1);
        //when
        //then
        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> parkingBoy.fetch(parkingTicket1));
        assertEquals("Unrecognized parking ticket", exception.getMessage());

    }

    @Test
    public void should_return_a_parking_ticket_of_first_parking_lot_when_park_given_a_car_and_two_parking_lots_with_first_parking_lot_has_available_position() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        Car car = new Car();

        //when
        ParkingTicket parkingTicket = parkingBoy.park(car);

        //then
        Assertions.assertTrue(parkingLot1.checkValidParkingTicket(parkingTicket));

    }

    @Test
    public void should_return_a_parking_ticket_of_second_parking_lot_when_park_given_a_car_and_two_parking_lots_with_first_parking_lot_no_available_position() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        Car car = new Car();

        //when
        ParkingTicket parkingTicket = parkingBoy.park(car);

        //then
        Assertions.assertTrue(parkingLot2.checkValidParkingTicket(parkingTicket));

    }

    @Test
    public void should_return_two_right_car_when_fetch_given_two_parking_tickets_and_two_parking_lots() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        Car car1 = new Car();
        Car car2 = new Car();

        ParkingTicket parkingTicket1 = parkingBoy.park(car1);
        ParkingTicket parkingTicket2 = parkingBoy.park(car2);

        //when
        Car returnCar1 = parkingBoy.fetch(parkingTicket1);
        Car returnCar2 = parkingBoy.fetch(parkingTicket2);

        //then
        Assertions.assertEquals(car1, returnCar1);
        Assertions.assertEquals(car2, returnCar2);

    }


    @Test
    public void should_return_Unrecognized_parking_ticket_when_fetch_given_two_parking_lots_and_a_Unrecognized_parking_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        //when
        //then
        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> parkingBoy.fetch(new ParkingTicket()));
        assertEquals("Unrecognized parking ticket", exception.getMessage());

    }

    @Test
    public void should_return_Unrecognized_parking_ticket_when_fetch_given_two_parking_lots_and_a_used_parking_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot();
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        ParkingTicket parkingTicket = parkingBoy.park(new Car());
        parkingBoy.fetch(parkingTicket);
        //when
        //then
        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> parkingBoy.fetch(parkingTicket));
        assertEquals("Unrecognized parking ticket", exception.getMessage());

    }


    @Test
    public void should_return_no_available_position_when_park_given_two_parking_lots_and_a_car_but_two_parking_lots_is_full() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(0);
        List<ParkingLot> parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot1);
        parkingLotList.add(parkingLot2);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLotList);
        Car car = new Car();

        //when
        //then
        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> parkingBoy.park(car));
        assertEquals("No available position", exception.getMessage());

    }
}
