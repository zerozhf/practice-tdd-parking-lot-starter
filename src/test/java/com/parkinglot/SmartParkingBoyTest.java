package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;


public class SmartParkingBoyTest {

    @Test
    public void should_return_a_parking_ticket_of_first_parking_lot_when_park_given_a_car_and_two_parking_lots_with_first_parking_lot_has_more_available_position() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot2.park(new Car());
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();

        //when
        ParkingTicket parkingTicket = smartParkingBoy.park(car);

        //then
        Assertions.assertTrue(parkingLot1.checkValidParkingTicket(parkingTicket));

    }


    @Test
    public void should_return_a_parking_ticket_of_second_parking_lot_when_park_given_a_car_and_two_parking_lots_with_second_parking_lot_has_more_available_position() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        parkingLot1.park(new Car());
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();

        //when
        ParkingTicket parkingTicket = smartParkingBoy.park(car);

        //then
        Assertions.assertTrue(parkingLot2.checkValidParkingTicket(parkingTicket));

    }

    @Test
    public void should_return_two_right_car_when_fetch_given_two_parking_tickets_and_two_parking_lots() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car1 = new Car();
        Car car2 = new Car();

        ParkingTicket parkingTicket1 = smartParkingBoy.park(car1);
        ParkingTicket parkingTicket2 = smartParkingBoy.park(car2);

        //when
        Car returnCar1 = smartParkingBoy.fetch(parkingTicket1);
        Car returnCar2 = smartParkingBoy.fetch(parkingTicket2);

        //then
        Assertions.assertEquals(car1, returnCar1);
        Assertions.assertEquals(car2, returnCar2);

    }

    @Test
    public void should_return_Unrecognized_parking_ticket_when_fetch_given_two_parking_lots_and_a_Unrecognized_parking_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        //when
        //then
        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> smartParkingBoy.fetch(new ParkingTicket()));
        assertEquals("Unrecognized parking ticket", exception.getMessage());

    }

    @Test
    public void should_return_Unrecognized_parking_ticket_when_fetch_given_two_parking_lots_and_a_used_parking_ticket() {
        //given
        ParkingLot parkingLot1 = new ParkingLot();
        ParkingLot parkingLot2 = new ParkingLot();
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        ParkingTicket parkingTicket = smartParkingBoy.park(new Car());
        smartParkingBoy.fetch(parkingTicket);
        //when

        //then
        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> smartParkingBoy.fetch(parkingTicket));
        assertEquals("Unrecognized parking ticket", exception.getMessage());

    }


    @Test
    public void should_return_no_available_position_when_park_given_two_parking_lots_and_a_car_but_two_parking_lots_is_full() {
        //given
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(0);
        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLot1, parkingLot2);
        Car car = new Car();

        //when
        //then
        Exception exception = Assertions.assertThrows(RuntimeException.class, () -> smartParkingBoy.park(car));
        assertEquals("No available position", exception.getMessage());

    }
}
