package com.parkinglot;

import java.util.HashMap;
import java.util.Map;

public class ParkingLot {

    private int capacity;

    private Map<ParkingTicket, Car> parkingTicketCarMap;

    public ParkingLot(int capacity) {
        this.capacity = capacity;
        parkingTicketCarMap = new HashMap<>();
    }

    public ParkingLot() {
        capacity = 10;
        parkingTicketCarMap = new HashMap<>();
    }

    public ParkingTicket park(Car car) {
        if (isFull()) {
            throw new RuntimeException("No available position");
        }
        ParkingTicket parkingTicket = new ParkingTicket();
        parkingTicketCarMap.put(parkingTicket, car);
        return parkingTicket;
    }

    public boolean isFull(){
        return parkingTicketCarMap.size() == capacity;
    }

    public boolean checkValidParkingTicket(ParkingTicket parkingTicket){
        return parkingTicketCarMap.containsKey(parkingTicket);
    }

    public Car fetch(ParkingTicket parkingTicket) {
        if (!checkValidParkingTicket(parkingTicket))
            throw new RuntimeException("Unrecognized parking ticket");
        Car car = parkingTicketCarMap.get(parkingTicket);
        parkingTicketCarMap.remove(parkingTicket);
        return car;
    }

    public int getRemainingParkingSpaces(){
        return capacity - parkingTicketCarMap.size();
    }

    public double getAvailablePositionRate(){
        return (double) (capacity - parkingTicketCarMap.size()) / capacity;
    }
}
