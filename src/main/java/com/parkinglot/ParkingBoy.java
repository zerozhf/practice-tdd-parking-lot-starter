package com.parkinglot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ParkingBoy {

    private List<ParkingLot> parkingLotList;

    public ParkingBoy(ParkingLot parkingLot) {
        this.parkingLotList = new ArrayList<>();
        parkingLotList.add(parkingLot);
    }

    public ParkingBoy(ParkingLot... parkingLots) {
        parkingLotList = new ArrayList<>();
        Arrays.stream(parkingLots).forEach(parkingLotList::add);
    }

    public ParkingBoy(List<ParkingLot> parkingLotList) {
        this.parkingLotList = parkingLotList;
    }

    public ParkingTicket park(Car car) {
        return parkingLotList.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .findFirst()
                .orElseThrow(() -> new RuntimeException("No available position"))
                .park(car);
    }


    public Car fetch(ParkingTicket parkingTicket) {
        return parkingLotList.stream()
                .filter(parkingLot -> parkingLot.checkValidParkingTicket(parkingTicket))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Unrecognized parking ticket"))
                .fetch(parkingTicket);
    }

}
