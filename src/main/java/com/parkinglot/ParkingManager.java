package com.parkinglot;

import java.util.Arrays;
import java.util.List;

public class ParkingManager extends ParkingBoy{
    private final List<ParkingBoy> parkingBoyList ;

    ParkingManager(List<ParkingBoy> parkingBoys,ParkingLot ...parkingLots){
        super(parkingLots);
        parkingBoyList = parkingBoys;
    }
}
