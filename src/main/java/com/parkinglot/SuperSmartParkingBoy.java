package com.parkinglot;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class SuperSmartParkingBoy extends ParkingBoy{
    private final List<ParkingLot> parkingLotList;

    public SuperSmartParkingBoy(ParkingLot... parkingLots) {
        parkingLotList = new ArrayList<>();
        Arrays.stream(parkingLots).forEach(parkingLotList::add);
    }


    public ParkingTicket park(Car car) {
        return parkingLotList.stream()
                .max(Comparator.comparingDouble(ParkingLot::getAvailablePositionRate))
                .get()
                .park(car);
    }


    public Car fetch(ParkingTicket parkingTicket) {
        return parkingLotList.stream()
                .filter(parkingLot -> parkingLot.checkValidParkingTicket(parkingTicket))
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Unrecognized parking ticket"))
                .fetch(parkingTicket);
    }
}
